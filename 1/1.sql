DROP TABLE IF EXISTS user_list, friend_list;

CREATE TABLE user_list (
  id   SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE friend_list (
  user_id   INTEGER NOT NULL REFERENCES user_list (id),
  friend_id INTEGER NOT NULL REFERENCES user_list (id),
  PRIMARY KEY (user_id, friend_id)
);

INSERT INTO user_list (name) VALUES
  ('u1'),
  ('u2'),
  ('u3'),
  ('u4'),
  ('u5'),
  ('u6'),
  ('u7'),
  ('u8'),
  ('u9');

INSERT INTO friend_list (user_id, friend_id) VALUES
  (1, 2),
  (1, 3),
  (1, 4),
  (1, 5),
  (1, 6),
  (1, 7),
  (2, 1),
  (2, 3),
  (2, 4),
  (2, 5),
  (2, 6),
  (2, 7),
  (3, 1),
  (3, 2),
  (3, 9),
  (4, 5),
  (5, 4);

SELECT *
FROM user_list
WHERE (SELECT COUNT(user_id)
       FROM friend_list
       WHERE user_list.id = friend_list.user_id) > 5;

SELECT
  user_list1.name as friend1,
  user_list2.name as friend2
FROM friend_list friend_list1
  JOIN friend_list friend_list2 ON friend_list1.user_id = friend_list2.friend_id
                                   AND friend_list1.friend_id = friend_list2.user_id
  JOIN user_list user_list1 ON user_list1.id = friend_list1.user_id
  JOIN user_list user_list2 ON user_list2.id = friend_list2.user_id
WHERE friend_list1.user_id < friend_list1.friend_id;
