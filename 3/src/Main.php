<?php

namespace App;

use App\HtmlLoader\HtmlFromUrlLoader;
use App\HtmlParser\HtmlParser;

class Main
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var HtmlParser
     */
    private $htmlParser;

    public function __construct()
    {
        $this->url = $_SERVER['argv'][1];
        $htmlLoader = new HtmlFromUrlLoader();
        $this->htmlParser = new HtmlParser($htmlLoader);
    }

    public function displayCountOfTagsInSource()
    {
        $res = $this->htmlParser->countTagsFromSource($this->url)->getResult();
        foreach ($res as $key => $item) {
            echo "$key: $item\n";
        }
    }
}