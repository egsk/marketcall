<?php

namespace App\HtmlParser;

use App\HtmlLoader\HtmlLoaderInterface;

class HtmlParser
{
    /**
     * @var string
     */
    private $data;

    /**
     * @var HtmlLoaderInterface
     */
    private $htmlLoader;

    /**
     * @var array
     */
    private $result;

    private const COMMENT_PATTERN = "~<!--(.*)-->~isU";
    private const SCRIPT_PATTERN = "~<script(.*)<\/script>~isU";
    private const GENERAL_PATTERN = "~<[^/>][^>]*>~isU";
    private const VALID_TAG_PATTERN = "~<[a-z]+[a-z0-9]*~i";

    public function __construct(HtmlLoaderInterface $htmlLoader)
    {
        $this->htmlLoader = $htmlLoader;
    }

    public function getResult(): ?array
    {
        return $this->result;
    }

    public function countTagsFromSource(string $source): self
    {
        $this->data = $this->htmlLoader->loadData($source)->getData();
        $this->countTags();

        return $this;
    }

    private function countTags(): self
    {
        $this->result = [];
        $this->countSpecificTag("comment", self::COMMENT_PATTERN);
        $this->data = preg_replace(self::COMMENT_PATTERN, "", $this->data);
        $this->countSpecificTag("script", self::SCRIPT_PATTERN);
        $this->data = preg_replace(self::SCRIPT_PATTERN, "", $this->data);
        $this->countAndNameTagsByPattern(self::GENERAL_PATTERN);

        return $this;
    }

    private function countSpecificTag($name, $pattern)
    {
        $tagCount = preg_match_all($pattern, $this->data);
        if ($tagCount > 0) {
            $this->result[$name] = $tagCount;
        }
    }

    private function countAndNameTagsByPattern($pattern)
    {
        $generalMatches = [];
        preg_match_all($pattern, $this->data, $generalMatches);
        foreach ($generalMatches[0] as $value) {
            preg_match(self::VALID_TAG_PATTERN, $value, $m);
            $localMatch = isset($m[0]) ? trim(strtolower($m[0]), "<") : false;
            if ($localMatch) {
                isset($this->result[$localMatch]) ?
                    $this->result[$localMatch]++ :
                    $this->result[$localMatch] = 1;
            }
        }
    }
}