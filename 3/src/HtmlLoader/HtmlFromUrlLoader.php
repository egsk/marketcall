<?php

namespace App\HtmlLoader;

use App\Exception\WrongResourceException;

class HtmlFromUrlLoader implements HtmlLoaderInterface
{
    /**
     * @var string
     */
    private $data;

    public function getData(): ?string
    {
        return $this->data;
    }

    /**
     * @param $source
     * @return HtmlLoaderInterface
     * @throws WrongResourceException
     */
    public function loadData($source): HtmlLoaderInterface
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $source);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        if ($response === false) {
            throw new WrongResourceException();
        }
        $this->data = $response;
        return $this;
    }
}