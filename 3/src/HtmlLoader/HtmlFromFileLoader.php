<?php

namespace App\HtmlLoader;

class HtmlFromFileLoader implements HtmlLoaderInterface
{
    /**
     * @var string
     */
    private $data;

    public function loadData($source): HtmlLoaderInterface
    {
        $this->data = file_get_contents($source);

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }
}