<?php

namespace App\HtmlLoader;

interface HtmlLoaderInterface
{
    public function loadData($source): self;
    public function getData(): ?string;
}