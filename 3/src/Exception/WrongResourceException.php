<?php

namespace App\Exception;

class WrongResourceException extends \Exception
{
    protected $message = "Given resource is wrong or unavailable";
}