<?php

namespace App\Test;

use App\HtmlLoader\HtmlFromFileLoader;
use App\HtmlParser\HtmlParser;
use PHPUnit\Framework\TestCase;

class HtmlCountTagsTest extends TestCase
{
    /**
     * @test
     */
    public function basic()
    {
        $loader = new HtmlFromFileLoader();
        $parser = new HtmlParser($loader);

        $val = $parser->countTagsFromSource(__DIR__ . "/fixtures/basic.html")->getResult();
        $this->assertEquals(array("h1" => 1), $val);

        $val = $parser->countTagsFromSource(__DIR__ . "/fixtures/middle.html")->getResult();
        $this->assertEquals(
            array(
                "comment" => 1,
                "h1" => 1,
                "span" => 1,
                "br" => 1,
            ), $val);

        $val = $parser->countTagsFromSource(__DIR__ . "/fixtures/advanced.html")->getResult();
        $this->assertEquals(array(
            "script" => 1,
            "comment" => 2,
            "p" => 2,
            "span" => 2,
        ), $val);
    }

}