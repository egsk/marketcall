<?php

$lines = file(__DIR__ . "/in.txt");
$f = fopen(__DIR__ . "/out.txt", "w");

foreach ($lines as $line) {
    $a = array_map('intval', explode(" ", $line));
//    bubble($a);
    qsort($a, 0, count($a) - 1);
    fwrite($f, implode(" ", $a) . "\n");
}

fclose($f);

/**
 * Реализация заданной в условии задачи функции
 * @param $arr array исходный массив
 * @param $index int индекс обмениваемого элемента
 */
function array_swap(&$arr, $index)
{
    list($arr[0], $arr[$index]) = array($arr[$index], $arr[0]);
}

/**
 * Обёртка над array_swap, позволяющая поменять
 * местами два значения массива по их индексам,
 * используя array_swap вместо присваиваний.
 * @param $arr array исходный массив
 * @param $i int индекс первого элемента
 * @param $j int индекс второго элемента
 */
function swap_two_values(&$arr, $i, $j)
{
    if ($i === $j) return;
    if ($i > 0 && $j > 0) {
        array_swap($arr, $i);
        array_swap($arr, $j);
        array_swap($arr, $i);
    } elseif ($i === 0) {
        array_swap($arr, $j);
    } else {
        array_swap($arr, $i);
    }
}

/**
 * Быстрая сортировка
 * @param $arr []int исходный массив
 * @param $firstIndex int индекс начала сортировки
 * @param $lastIndex int индекс конца сортировки
 */
function qsort(&$arr, $firstIndex, $lastIndex)
{
    $i = $firstIndex;
    $j = $lastIndex;
    $direction = true;
    while ($i < $j) {
        if ($arr[$i] > $arr[$j]) {
            swap_two_values($arr, $i, $j);
            $direction = !$direction;
        }
        if ($direction) {
            $j--;
        } else {
            $i++;
        }
    }
    $k = $i;
    if ($firstIndex < $lastIndex) {
        qsort($arr, $firstIndex, $k - 1);
    }
    if ($i < $lastIndex) {
        qsort($arr, $k + 1, $lastIndex);
    }
}
